function showAlertTempInt() {
    let sectionInt = document.getElementById('detailsTempInt');
    let buttonInt = document.getElementById('buttonTempInt');
    if(sectionInt.style.display === 'none') {
        sectionInt.style.display = 'block';
        buttonInt.style.display = 'none';
    } else {
        sectionInt.style.display = 'none';
        buttonInt.style.display = 'block';
    }
}

function showAlertTempExt() {
    let sectionExt = document.getElementById('detailsTempExt');
    let buttonExt = document.getElementById('buttonTempExt');
    if(sectionExt.style.display === 'none') {
        sectionExt.style.display = 'block';
        buttonExt.style.display = 'none';
    } else {
        sectionExt.style.display = 'none';
        buttonExt.style.display = 'block';
    }
}

function showAlertBatInt() {
    let sectionInt = document.getElementById('detailsBatInt');
    let buttonInt = document.getElementById('buttonBatInt');
    if(sectionInt.style.display === 'none') {
        sectionInt.style.display = 'block';
        buttonInt.style.display = 'none';
    } else {
        sectionInt.style.display = 'none';
        buttonInt.style.display = 'block';
    }
}

function showAlertBatExt() {
    let sectionExt = document.getElementById('detailsBatExt');
    let buttonExt = document.getElementById('buttonBatExt');
    if(sectionExt.style.display === 'none') {
        sectionExt.style.display = 'block';
        buttonExt.style.display = 'none';
    } else {
        sectionExt.style.display = 'none';
        buttonExt.style.display = 'block';
    }
}