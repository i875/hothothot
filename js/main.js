
function setString(value) {
    if (value < 10) {
        value = '0' + value;
    }
    return value;
}

function setHour() {
    let date = new Date();

    let hour = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();

    document.getElementById('time').innerHTML = 'Il est actuellement ' + setString(hour) + 'h ' + setString(minutes) + 'min ' + setString(seconds) + 's';

    return hour;
}

function getCurrentValues(data, hour){
    let currentData;
    data.forEach(element => {
        if (element.hour == hour){
            currentData = element;
        }
    });
    return currentData;
}

function getTempExt(data){
    let temp_ext = [];
    data.forEach(element => {
        temp_ext.push(element.temp_ext)
    });
    return temp_ext;
}

function getTempInt(data){
    let temp_int = [];
    data.forEach(element => {
        temp_int.push(element.temp_int)
    });
    return temp_int;
}

function getBatExt(data){
    let bat_ext = [];
    data.forEach(element => {
        bat_ext.push(element.bat_ext)
    });
    return bat_ext;
}

function getBatInt(data){
    let bat_int = [];
    data.forEach(element => {
        bat_int.push(element.bat_int)
    });
    return bat_int;
}

function getTempExtMin(data){
    let temp_min = data[0].temp_ext;
    data.forEach(element => {
        if (element.temp_ext < temp_min){
            temp_min = element.temp_ext;
        }
    });
    return temp_min;
}

function getTempExtMax(data){
    let temp_max = data[0].temp_ext;
    data.forEach(element => {
        if (element.temp_ext > temp_max){
            temp_max = element.temp_ext;
        }
    });
    return temp_max;
}

function getTempIntMin(data){
    let temp_min = data[0].temp_int;
    data.forEach(element => {
        if (element.temp_int < temp_min){
            temp_min = element.temp_int;
        }
    });
    return temp_min;
}

function getTempIntMax(data){
    let temp_max = data[0].temp_int;
    data.forEach(element => {
        if (element.temp_int > temp_max){
            temp_max = element.temp_int;
        }
    });
    return temp_max;
}

function setTemp(data){
    document.getElementById('tempExt').innerHTML = data.temp_ext; 
    document.getElementById('tempInt').innerHTML = data.temp_int;
}

function setBat(data){
    document.getElementById('batExt').innerHTML = data.bat_ext; 
    document.getElementById('batInt').innerHTML = data.bat_int;
}

function setTempMinMax(data){
    document.getElementById('tempExtMin').innerHTML = getTempExtMin(data);
    document.getElementById('tempExtMax').innerHTML = getTempExtMax(data);
    document.getElementById('tempIntMin').innerHTML = getTempIntMin(data);
    document.getElementById('tempIntMax').innerHTML = getTempIntMax(data);
}

function setGraphs(data){
    let temp_ext = getTempExt(data);
    let temp_int = getTempInt(data);
    let bat_ext = getBatExt(data);
    let bat_int = getBatInt(data);
    const hours = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

    setTempGraph(temp_ext, temp_int, hours);
    setBatGraph(bat_ext, bat_int, hours);

}

function setTempGraph(temp_ext, temp_int, hours){
    const canvasTemp = document.getElementById('graphTemp').getContext('2d');
    const graphTemp = new Chart(canvasTemp, {
        type: 'radar',
        data: {
            labels: hours,
            datasets: [{
                label: 'Exterieur',
                data: temp_ext,
                backgroundColor: 'rgba(255, 99, 132, 1)',
                borderColor: 'rgba(255, 99, 132, 1)',
                fill: false
                }, {
                label: 'Interieur',
                data: temp_int,
                backgroundColor: 'rgba(54, 162, 235, 1)',
                borderColor: 'rgba(54, 162, 235, 1)',
                fill: false
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
                display: true,
                text: "Temperatures sur les dernieres 24h"
            }
        }
    });
}

function setBatGraph(bat_ext, bat_int, hours){
    const canvasBat = document.getElementById('graphBat').getContext('2d');
    const graphBat = new Chart(canvasBat, {
        type: 'radar',
        data: {
            labels: hours,
            datasets: [{
                label: 'Exterieur',
                data: bat_ext,
                backgroundColor: 'rgba(255, 99, 132, 1)',
                borderColor: 'rgba(255, 99, 132, 1)',
                fill: false
                }, {
                label: 'Interieur',
                data: bat_int,
                backgroundColor: 'rgba(54, 162, 235, 1)',
                borderColor: 'rgba(54, 162, 235, 1)',
                fill: false
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
                display: true,
                text: "% de batterie sur les dernieres 24h"
            }
        }
    });
}

function setAlerts(alertsData, currentData){

    let temp_ext = currentData.temp_ext;
    let temp_int = currentData.temp_int;
    let bat_ext = currentData.bat_ext;
    let bat_int = currentData.bat_int;

    let alertsExt = alertsData.capteurExt;
    let alertsInt = alertsData.capteurInt;
    let alertsBat = alertsData.batterie;

    setAlertsTempExt(temp_ext, alertsExt);
    setAlertsTempInt(temp_int, alertsInt);
    setAlertsBatExt(bat_ext, alertsBat);
    setAlertsBatInt(bat_int, alertsBat);

}

function setAlertsTempExt(temp_ext, alertsExt){
    if (temp_ext <= 0) {
        document.getElementById('alertTempExt').innerHTML = alertsExt[0][0];
    } else if (temp_ext >= 35) {
        document.getElementById('alertTempExt').innerHTML = alertsExt[0][35];
    } else {
        document.getElementById('alertTempExt').innerHTML = 'Aucune alerte';
    }
}

function setAlertsTempInt(temp_int, alertsInt){
    if (temp_int <= 12) {
        if (temp_int <= 0) {
            document.getElementById('alertTempInt').innerHTML = alertsInt[0][0];
        } else {
            document.getElementById('alertTempInt').innerHTML = alertsInt[0][12];
        }
    } else if (temp_int >= 22) {
        if (temp_int >= 50) {
            document.getElementById('alertTempInt').innerHTML = alertsInt[0][50];
        } else {
            document.getElementById('alertTempInt').innerHTML = alertsInt[0][22];
        }
    } else {
        document.getElementById('alertTempInt').innerHTML = 'Aucune alerte';
    }
}

function setAlertsBatExt(bat_ext, alertsBat){
    if (bat_ext <= 50) {
        if (bat_ext <= 25) {
            if (bat_ext == 0){
                document.getElementById('alertBatExt').innerHTML = alertsBat[0][0];
            } else {
                document.getElementById('alertBatExt').innerHTML = alertsBat[0][25];
            }
        } else {
            document.getElementById('alertBatExt').innerHTML = alertsBat[0][50];
        }
    } else if (bat_ext <= 100) {
        if (bat_ext <= 75) {
            document.getElementById('alertBatExt').innerHTML = alertsBat[0][75];
        } else {
            document.getElementById('alertBatExt').innerHTML = alertsBat[0][100];
        }
    }
}

function setAlertsBatInt(bat_int, alertsBat){
    if (bat_int <= 50) {
        if (bat_int <= 25) {
            if (bat_int == 0){
                document.getElementById('alertBatInt').innerHTML = alertsBat[0][0];
            } else {
                document.getElementById('alertBatInt').innerHTML = alertsBat[0][25];
            }
        } else {
            document.getElementById('alertBatInt').innerHTML = alertsBat[0][50];
        }
    } else if (bat_int <= 100) {
        if (bat_int <= 75) {
            document.getElementById('alertBatInt').innerHTML = alertsBat[0][75];
        } else {
            document.getElementById('alertBatInt').innerHTML = alertsBat[0][100];
        }
    }
}

setInterval(async function () {
    
    let data;
    await fetch('../json/data.json')
        .then(response => response.json())
        .then(json => {
            data = json.data
        });

    let hour = setHour();
    let currentData = getCurrentValues(data, hour)
    setTemp(currentData);
    setBat(currentData);
    setTempMinMax(data);
    setGraphs(data);

    let alertsData;
    await fetch('../json/alerts.json')
        .then(response => response.json())
        .then(json => {
            alertsData = json.capteurs[0];
        });

    setAlerts(alertsData, currentData);
    // console.log(data);

}, 1000);